Moderate public posts from organic groups module. Moderated posts will
be hidden from anonymous visitors. Moderation needs to be done by node
administrators or another module.

Author: Gerhard Killesreiter
